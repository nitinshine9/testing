//
//  modalNew.swift
//  Testing App
//
//  Created by Nitin Mittal on 11/09/18.
//  Copyright © 2018 Nitin Mittal. All rights reserved.
//

import UIKit
import SwiftyJSON

class modalNew: NSObject {
    
    
    var userId : String?
    var postId : String?
    
    var title : String?
    var postInfo: String?
    var username : String?
    var profileImage: String?
    
    var email: String?
    var gender: String?
    var phone: String?
    
    var totalComments: Int?
    var totalRetweet: Int?
    var totalLike: Int?
    
    var createdTime: String?
    
    var isLiked: String?
    
    var postImages : [Any]?
    
    override init(){
        super.init()
    }
    
    init(userInfoJSON : JSON?){
        super.init()
        
        fillInfo(info: userInfoJSON)
    }
    
    func fillInfo(info : JSON?){
        
        guard let info = info
            else{
                return
        }
        
        postId = info["post_id"].stringValue
        userId = info["user1_id"].stringValue
        
        title = info["title"].stringValue
        postInfo = info["description"].stringValue
        username = info["business_name"].stringValue
        profileImage = info["profile_image"].stringValue
        email = info["email"].stringValue
        gender = info["gender"].stringValue
        phone = info["phone"].stringValue
        postImages = info["post_images"].array
        
        totalComments = info["total_comments"].intValue
        totalRetweet = info["total_tweets"].intValue
        totalLike = info["total_likes"].intValue
        
        createdTime = info["created_at"].stringValue
        
    }
    
    
    

}
